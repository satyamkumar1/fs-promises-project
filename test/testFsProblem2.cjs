const createCallbackFunction = require("../fs-problem2.cjs");
const path = require('path');



const lipsumfile = path.resolve('lipsum.txt');

createCallbackFunction(lipsumfile, (err) => {
    if (err) {
        console.log(err);
    }
    else {
        console.log("great");
    }
});
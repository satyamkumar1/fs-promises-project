/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/




function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {


    const fs = require('fs');

    const jsonFileArray = [];


    const makeDirectory = new Promise((resolve, reject) => {

        return fs.mkdir(absolutePathOfRandomDirectory, (err) => {
            if (err) {

                reject(err);
            }
            else {

                resolve("directory is created");

            }
        });
    })
    makeDirectory.then((getData) => {

        console.log(getData);

        const createFile = new Promise((resolve, reject) => {

            for (let index = 1; index <= randomNumberOfFiles; index++) {

                const jsonFile = `created${index}.json`

                fs.writeFile(`${absolutePathOfRandomDirectory}/${jsonFile}`, "this is good", (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {

                        jsonFileArray.push(jsonFile);
                        console.log("file is created");

                        if (jsonFileArray.length === randomNumberOfFiles) {
                            resolve("successful all file created");
                        }
                    }

                });
            }

        });
        createFile.then((data) => {
            console.log(data);

            deleteFile(absolutePathOfRandomDirectory);

        }).catch((err) => {
            console.log(err);
        })

    });



    function deleteFile(absolutePathOfRandomDirectory) {

        for (let index = 0; index < jsonFileArray.length; index++) {

            fs.unlink(`${absolutePathOfRandomDirectory}/${jsonFileArray[index]}`, (err) => {
                if (err) {
                    console.log("no file" + err);
                }
                else {
                    console.log("file is deleted");

                }
            });

        }
    }


}

module.exports = fsProblem1;




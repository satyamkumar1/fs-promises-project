/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

function createCallbackFunction(lipsum, cb) {

    const fs = require('fs');

    const readData = readingFile(lipsum);

    readData.then((data) => {
        console.log(data);
        return writingFile(data.toUpperCase(), 'upperCase.txt')
    }).then((data) => {
        console.log(data);
        return appendingFile('upperCase.txt' + '\n');
    }).then((data) => {
        console.log(data);
        return writingFile(data.toLocaleLowerCase().split('. ').join('\n'), './lowerCase.txt');
    }).then((data) => {
        console.log(data);
        return appendingFile('lowerCase.txt' + '\n');
    }).then((data) => {
        console.log("reading file");
        return readingFile('./lowerCase.txt');
    }).then((data) => {
        console.log("writing sorted data");
        return writingFile(data.split('\n').sort().join('\n'), './sortedData.txt');
    }).then((data) => {
        console.log("append succesful");
        return appendingFile('sortedData.txt');
    }).then((data) => {
        console.log("reading file");
        return readingFile('./filenames.txt');
    }).then((data) => {
        console.log("deleting data");
        return deletingFile(data);
    }).then((data) => {
        console.log(data);
    })





    function readingFile(lipsum) {
        return new Promise((reslove, reject) => {
            fs.readFile(lipsum, 'utf-8', (err, data) => {
                if (err) {
                    console.log(err);
                }
                else {
                    // console.log(data.toString());
                    reslove(data);
                }
            });
        })
    }

    function writingFile(data, filePath) {
        return new Promise((reslove, reject) => {

            fs.writeFile(filePath, data, (err, data) => {
                if (err) {
                    console.log(err);
                }
                else {

                    reslove("file is created and write");
                }
            });

        });

    }


    function appendingFile(filePath) {

        return new Promise((reslove, reject) => {

            fs.appendFile('filenames.txt', filePath, (err) => {
                if (err) {
                    console.log(err);
                }
                else {
                    reslove("file is append");
                }
            });
        });
    }
    function deletingFile(data) {
        return new Promise((resolve, reject) => {
            data = data.split('\n');
            data.map((file) => {
                fs.unlink(`${file}`, (err) => {
                    if (err) {
                        console.log(err);
                    }
                    //console.log(file);

                });
            });
            fs.unlink('filenames.txt', (err) => {
                if (err) {
                    console.log(err);
                } else {
                    resolve("succesful deleted");
                }
            });
        });
    }



}
module.exports = createCallbackFunction;